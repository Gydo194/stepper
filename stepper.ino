/**
 * Inductie harder spil aansturing
 * 10/01/2021
 * 
 * Gydo Kosten
 */
 
#include <AccelStepper.h>

/* stappenmotor aansluiting */
#define DIRECTION_PIN           11  /* stappenmotor dirver direction pin */
#define PULSE_PIN               10  /* stappenmotor driver pulse pin */
#define MOTOR_INTERFACE_TYPE    1   /* stappenmotor driver interface */

/* PLC interface */
#define RUN_ARDUINO_PIN         2 /* programma uitvoeren */
#define STARTPOSITIE_PIN        3 /* ga nu naar start pos (home) */
#define STARTPOSITIE_OUTPUT_PIN 4 /* arduino -> plc: sta nu in start positie */
#define PRESET_1_PIN            5 /* start cyclys 1 */
#define PRESET_2_PIN            6 /* start cyclus 2 */
#define RUSTPOSITIE_SENSOR_PIN  7 /* startpositie sensor: hoog is in startpositie */
#define KLAAR_PIN               8 /* arduino -> plc: programma klaar */
#define READY_PIN               9 /* arduino -> plc: kan beginnen met harden */

#define STARTPOSITIE_BENADERING_SNELHEID  500.0 /* snelheid bij benaderen sensor */
#define ZAK_SNELHEID  1000.0 /* snelheid naar beneden voor een cyclus */

#define EINDPOSITIE_STAPPEN 3000 /* aantal stappen naar helemaal beneden */
#define HOMING_MAX_STAPPEN  5000 /* maximum aantal stappen te nemen bij het homen */

#define PRESET_1_STAPPEN  2000  /* preset 1 aantal stappen let op; absolute waarde! */
#define PRESET_1_SNELHEID 200.0 /* preset 1 snelheid */

#define PRESET_2_STAPPEN 1000   /* preset 2 aantal stappen. let op; absolute waarde! */
#define PRESET_2_SNELHEID 500.0 /* preset 2 snelheid */

#define ACCELERATIE 1000.0  /* acceleratie waarde */

/* 1: IO pinnen in acht nemen; 0: IO pinnen negeren */
#define QUERY_PINS 1

/* seriele poort snelheid */
#define SERIAL_BAUDRATE 9600

/**
 * Wanneer preset signaal gegeven wordt naar beneden gaan
 * dan wanneer run arduino signaal gegeven wordt duty cycle uitvoeren
 * dan wanneer startpositie signaal gegeven wordt terug naar boven gaan
 */
#define STATE_IDLE    0
#define STATE_HOMING  1

#define STATE_CYCLE_1_START 2
#define STATE_CYCLE_2_START 3

#define STATE_CYCLE_1_WAIT_DUTY 4
#define STATE_CYCLE_2_WAIT_DUTY 5

#define STATE_CYCLE_1_DUTY  6
#define STATE_CYCLE_2_DUTY  7

/* stappenmotor control */
AccelStepper stepper = AccelStepper(MOTOR_INTERFACE_TYPE, PULSE_PIN, DIRECTION_PIN);

/* huidige status */
byte state = STATE_IDLE;

/* serial input */
unsigned char serial_input = '0';

/* initializatie/opstart procedure */
void setup()
{
  /* IO pin richtingen input/output */
  pinMode(DIRECTION_PIN,            OUTPUT);
  pinMode(PULSE_PIN,                OUTPUT);
  pinMode(RUN_ARDUINO_PIN,          INPUT);
  pinMode(STARTPOSITIE_PIN,         INPUT);
  pinMode(STARTPOSITIE_OUTPUT_PIN,  OUTPUT);
  pinMode(PRESET_1_PIN,             INPUT);
  pinMode(PRESET_2_PIN,             INPUT);
  pinMode(RUSTPOSITIE_SENSOR_PIN,   INPUT);
  pinMode(KLAAR_PIN,                OUTPUT);

  /* stappenmotor initializatie */
  stepper.setAcceleration(ACCELERATIE);

  /* output signalen initializatie */
  digitalWrite(STARTPOSITIE_OUTPUT_PIN, LOW);
  digitalWrite(KLAAR_PIN, HIGH);
  digitalWrite(READY_PIN, LOW);

  /* start de seriele poort */
  Serial.begin(SERIAL_BAUDRATE);
  delay(500);
  Serial.println(F("START"));
}

/* start de homing procedure */
void start_homing()
{
    Serial.println(F("Start homing"));
    stepper.setMaxSpeed(STARTPOSITIE_BENADERING_SNELHEID);
    stepper.setCurrentPosition(HOMING_MAX_STAPPEN);
    stepper.moveTo(0);

    digitalWrite(STARTPOSITIE_OUTPUT_PIN, LOW);
    
    state = STATE_HOMING;
}

/* idle status; doe niks maar ontvang binnenkomende opdrachten */
void sf_idle()
{
  /* idle -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }

  /* idle -> cycle_1_start */
#if QUERY_PINS
  if(HIGH == digitalRead(PRESET_1_PIN) || '2' == serial_input)
#else
  if('2' == serial_input)
#endif
  {
    Serial.println(F("START CYCLUS 1"));
    
    stepper.moveTo(EINDPOSITIE_STAPPEN);
    stepper.setMaxSpeed(ZAK_SNELHEID);
    
    digitalWrite(STARTPOSITIE_OUTPUT_PIN, LOW);
    digitalWrite(KLAAR_PIN, LOW);
    
    state = STATE_CYCLE_1_START;
    return;
  }

   /* idle -> cycle_2_start */
#if QUERY_PINS
  if(HIGH == digitalRead(PRESET_2_PIN) || '3' == serial_input)
#else
  if('3' == serial_input)
#endif
  {
    Serial.println(F("START CYCLUS 2"));
    
    stepper.moveTo(EINDPOSITIE_STAPPEN);
    stepper.setMaxSpeed(ZAK_SNELHEID);
    
    digitalWrite(STARTPOSITIE_OUTPUT_PIN, LOW);
    digitalWrite(KLAAR_PIN, LOW);
    
    state = STATE_CYCLE_2_START;
    return;
  }
}

/* homing procedure */
void sf_homing()
{ 
  /* sensor geraakt; stop homing */
#if QUERY_PINS
  if(HIGH == digitalRead(RUSTPOSITIE_SENSOR_PIN) || '4' == serial_input)
#else
  if('4' == serial_input)
#endif
  {
    Serial.println(F("Finished"));
    stepper.setCurrentPosition(0);
    stepper.stop();

    digitalWrite(STARTPOSITIE_OUTPUT_PIN, HIGH);
    
    state = STATE_IDLE;
    return;
  }

  /* hele slag gemaakt maar geen input van sensor; stop */
  if(0 == stepper.distanceToGo())
  {
    Serial.println(F("HOMING ERROR"));
    
    state = STATE_IDLE;
    return;
  }

  stepper.run();
}

/* cyclus 1 start */
void sf_cycle_1_start()
{
    /* cycle_1_start -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
  if(0 == stepper.distanceToGo())
  {
    digitalWrite(READY_PIN, HIGH);
    
    state = STATE_CYCLE_1_WAIT_DUTY;
    return;
  }
  
  stepper.run();
}

/* cyclus 2 start */
void sf_cycle_2_start()
{
    /* cycle_2_start -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
  if(0 == stepper.distanceToGo())
  {
    digitalWrite(READY_PIN, HIGH);
    
    state = STATE_CYCLE_2_WAIT_DUTY;
    return;
  }
  
  stepper.run();
}

/* cyclus 1 wachten op run_arduino signaal */
void sf_cycle_1_wait_duty()
{
    /* cycle_1_wait_duty -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
#if QUERY_PINS
  if(HIGH == digitalRead(RUN_ARDUINO_PIN) || '5' == serial_input)
#else
  if('5' == serial_input)
#endif
  {
    Serial.println(F("activate duty 1"));
    digitalWrite(READY_PIN, LOW);
    
    stepper.setMaxSpeed(PRESET_1_SNELHEID);
    stepper.moveTo(PRESET_1_STAPPEN);
    
    state = STATE_CYCLE_1_DUTY;
    return;
  }
}

/* cyclus 2 wachten op run_arduino signaal */
void sf_cycle_2_wait_duty()
{
  /* cycle_2_wait_duty -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
#if QUERY_PINS
  if(HIGH == digitalRead(RUN_ARDUINO_PIN) || '5' == serial_input)
#else
  if('5' == serial_input)
#endif
  {
    Serial.println(F("activate duty 2"));
    digitalWrite(READY_PIN, LOW);
    
    stepper.setMaxSpeed(PRESET_2_SNELHEID);
    stepper.moveTo(PRESET_2_STAPPEN);
    
    state = STATE_CYCLE_2_DUTY;
    return;
  }
}

/* cyclus 1 duty gedeelte */
void sf_cycle_1_duty()
{
    /* cycle_1_duty -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
   if(0 == stepper.distanceToGo())
   {
      Serial.println(F("Duty 1 complete"));
      
      digitalWrite(KLAAR_PIN, HIGH);
      state = STATE_IDLE;
      return;
   }

   stepper.run();
}

/* cyclus 2 duty gedeelte */
void sf_cycle_2_duty()
{
    /* cycle_2_duty -> homing */
#if QUERY_PINS
  if(HIGH == digitalRead(STARTPOSITIE_PIN) || '1' == serial_input)
#else
  if('1' == serial_input)
#endif
  {
    start_homing();
    return;
  }
  
   if(0 == stepper.distanceToGo())
   {
      Serial.println(F("Duty 2 complete"));
      
      digitalWrite(KLAAR_PIN, HIGH);
      state = STATE_IDLE;
      return;
   }

   stepper.run();
}

/* interpreteer de situatie afhankelijk van de huidige state */
void check_action()
{
  /* voer de juiste functie uit op basis van welke 'state' het systeem zich nu in bevindt */
  switch(state)
  {
    case STATE_IDLE:
      sf_idle();
      break;

    case STATE_HOMING:
      sf_homing();
      break;

    case STATE_CYCLE_1_START:
      sf_cycle_1_start();
      break;
      
    case STATE_CYCLE_2_START:
      sf_cycle_2_start();
      break;

    case STATE_CYCLE_1_WAIT_DUTY:
      sf_cycle_1_wait_duty();
      break;

    case STATE_CYCLE_2_WAIT_DUTY:
      sf_cycle_2_wait_duty();
      break;

    case STATE_CYCLE_1_DUTY:
      sf_cycle_1_duty();
      break;

    case STATE_CYCLE_2_DUTY:
      sf_cycle_2_duty();
      break;
  }
}

/* inlezen van commando via de seriele poort */
void read_serial()
{
  if(Serial.available() > 0)
  {
    serial_input = Serial.read();
  }
  else
  {
    serial_input = '0';
  }

  /* leeggooien van serial buffer */
  Serial.flush();
}

/* hoofd programma */
void loop()
{
  read_serial();
  check_action();
}
